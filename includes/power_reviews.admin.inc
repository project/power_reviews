<?php

/**
 * Setting the API configurations.
 */
function powerreviews_general_config() {
  $node_types = node_type_get_types();
  foreach ($node_types as $key => $value) {
    $node_option[$key] = $value->name;
  }

  $form = array();
  $form['powerreviews_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Power Review API Details'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['powerreviews_fieldset']['power_reviews_api_url'] = array(
    '#type' => 'textfield',
    '#title' => t('API URL'),
    '#required' => TRUE,
    '#default_value' => variable_get('power_reviews_api_url', ''),
  );

  $form['powerreviews_fieldset']['power_reviews_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#required' => TRUE,
    '#default_value' => variable_get('power_reviews_api_key', ''),
  );

  $form['powerreviews_fieldset']['power_reviews_merchant_group_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant Group ID'),
    '#required' => TRUE,
    '#default_value' => variable_get('power_reviews_merchant_group_id', ''),
  );

  $form['powerreviews_fieldset']['power_reviews_merchant_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant ID'),
    '#required' => TRUE,
    '#default_value' => variable_get('power_reviews_merchant_id', ''),
  );

  $form['powerreviews_fieldset']['power_reviews_locale'] = array(
    '#type' => 'textfield',
    '#title' => t('Locale'),
    '#required' => TRUE,
    '#default_value' => variable_get('power_reviews_locale', ''),
  );

  $form['powerreviews_fieldset']['power_reviews_feedless_implementation'] = array(
    '#type' => 'radios',
    '#title' => t('Is this a feedless implementation?'),
    '#required' => TRUE,
    '#options' => array(
      'yes' => t('Yes'),
      'no' => t('No'),
    ),
    '#default_value' => variable_get('power_reviews_feedless_implementation', ''),
  );

  $form['powerreviews_content_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content Type Options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['powerreviews_content_fieldset']['power_reviews_applied_content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select'),
    '#options' => $node_option,
    '#default_value' => variable_get('power_reviews_applied_content_types', []),
  );

  $form['powerreviews_content_fieldset']['power_reviews_applied_reviewsnippet'] = array(
    '#type' => 'textfield',
    '#title' => t('Review snippet'),
    '#description' => t('Add div ID where review snippet should appear.'),
    '#default_value' => variable_get('power_reviews_applied_reviewsnippet', ''),
  );

  $form['powerreviews_content_fieldset']['power_reviews_applied_reviewdisplay'] = array(
    '#type' => 'textfield',
    '#title' => t('Review display'),
    '#description' => t('Add div ID where review display should appear.'),
    '#default_value' => variable_get('power_reviews_applied_reviewdisplay', ''),
  );

  $form['powerreviews_content_fieldset']['power_reviews_applied_categorysnippet'] = array(
    '#type' => 'textfield',
    '#title' => t('Category snippet'),
    '#description' => t('Add div ID where review star summary should appear.'),
    '#default_value' => variable_get('power_reviews_applied_categorysnippet', ''),
  );

  $form['powerreviews_review_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Review Page'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['powerreviews_review_fieldset']['power_reviews_write_review_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to write a review page'),
    '#description' => t('Genreates the Review Form page.'),
    '#default_value' => variable_get('power_reviews_write_review_link', ''),
  );

  $form['powerreviews_review_fieldset']['power_reviews_write_review_page_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title for write a review page'),
    '#description' => t('Title for the review form page.'),
    '#default_value' => variable_get('power_reviews_write_review_page_title', ''),
  );

  $form['powerreviews_review_fieldset']['power_reviews_write_review'] = array(
    '#type' => 'textfield',
    '#title' => t('Write a review'),
    '#description' => t('Add div ID to show the review form.'),
    '#default_value' => variable_get('power_reviews_write_review', ''),
  );

  $form['powerreviews_review_fieldset']['power_reviews_custom_header'] = array(
    '#type' => 'radios',
    '#title' => t('Do you want a custom header?'),
    '#options' => array(
      'yes' => 'Yes',
      'no' => 'No',
    ),
    '#default_value' => variable_get('power_reviews_custom_header', []),
  );

  $form['powerreviews_review_fieldset']['power_reviews_custom_header_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title for Write a review page'),
    '#default_value' => variable_get('power_reviews_custom_header_title', ''),
    '#states' => array(
      'visible' => array(
        ':input[name="power_reviews_custom_header"]' => array('value' => 'yes'),
      ),
    ),
  );

  $form['powerreviews_review_fieldset']['power_reviews_custom_header_subtitle'] = array(
    '#type' => 'textfield',
    '#title' => t('Subtitle for Write a review page'),
    '#default_value' => variable_get('power_reviews_custom_header_subtitle', ''),
    '#states' => array(
      'visible' => array(
        ':input[name="power_reviews_custom_header"]' => array('value' => 'yes'),
      ),
    ),    
  );    

  return power_reviews_settings_form($form);
}

/**
 * Power Reviews setting form.
 */
function power_reviews_settings_form($form) {
  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration'));

  if (!empty($_POST) && form_get_errors()) {
    drupal_set_message(t('The settings have not been saved because of the errors.'), 'error');
  }
  $form['#submit'][] = 'power_reviews_settings_form_submit';
  // By default, render the form using theme_system_settings_form().
  if (!isset($form['#theme'])) {
    $form['#theme'] = 'power_reviews_settings_form';
  }
  return $form;
}

/**
 * Form submission handler for system_settings_form().
 *
 * If you want node type configure style handling of your checkboxes,
 * add an array_filter value to your form.
 */
function power_reviews_settings_form_submit($form, &$form_state) {
  $power_reviews_field_names = json_decode(variable_get('power_reviews_default_fields'), TRUE);
  foreach ($form_state['values']['power_reviews_applied_content_types'] as $content_type => $value) {
    power_reviews_update_fields($content_type, $value, $power_reviews_field_names, $form_state['values']['power_reviews_feedless_implementation']);
  }

  // Exclude unnecessary elements.
  form_state_values_clean($form_state);

  foreach ($form_state['values'] as $key => $value) {
    if (is_array($value) && isset($form_state['values']['array_filter'])) {
      $value = array_keys(array_filter($value));
    }
    variable_set($key, $value);
  }

  if($form_state['values']['power_reviews_custom_header'] == 'no'){
    variable_del('power_reviews_custom_header_title');
    variable_del('power_reviews_custom_header_subtitle');
  }  

  drupal_set_message(t('The configuration options have been saved.'));
}
