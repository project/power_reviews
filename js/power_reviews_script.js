(function ($) {
	Drupal.behaviors.power_reviews = {
	attach: function (context, settings) {
		var config = Drupal.settings.powerreview_api_details;
		var content_type_details = Drupal.settings.powerreviews_content_details;
		var powerreviews_review_details = Drupal.settings.powerreviews_review_details;
		if(content_type_details != undefined && content_type_details.power_reviews_applied_reviewsnippet != '') {
			POWERREVIEWS.display.render([
				{
					api_key: config.power_reviews_api_key,
					merchant_group_id: config.power_reviews_merchant_group_id,
					merchant_id: config.power_reviews_merchant_id,
					locale: config.power_reviews_locale,
					page_id: content_type_details.powerreviews_page_id,
					review_wrapper_url: config.power_reviews_write_review_url + '/?nid=' + content_type_details.nid + '&pr_page_id=' + content_type_details.powerreviews_page_id,
					product: content_type_details.power_reviews_product_details,
					on_render: function(config, data) {
					  setTimeout(function() {
					    var getText = jQuery('.pr-snippet-read-and-write .pr-snippet-review-count').text();
						if(getText && getText.indexOf('No Reviews') > -1) {
						  jQuery('.pr-snippet-read-and-write .pr-snippet-review-count, #power-review-content .cta-btn.buy-button').addClass('hide');
						}
					  }, 100);
					},
					components: {
						ReviewSnippet: content_type_details.power_reviews_applied_reviewsnippet,
						ReviewDisplay: content_type_details.power_reviews_applied_reviewdisplay
					}
				}
			]);
		}

		if(content_type_details != undefined && content_type_details.power_reviews_applied_categorysnippet != '') {
			POWERREVIEWS.display.render({
				api_key: config.power_reviews_api_key,
				merchant_group_id: config.power_reviews_merchant_group_id,
				merchant_id: config.power_reviews_merchant_id,
				locale: config.power_reviews_locale,
				page_id: content_type_details.powerreviews_page_id,
				review_wrapper_url: config.power_reviews_write_review_url + '/?pr_page_id=' + content_type_details.powerreviews_page_id,
				components: {
					CategorySnippet: content_type_details.power_reviews_applied_categorysnippet
				}
			});	
		}

		if(powerreviews_review_details != undefined) {
			POWERREVIEWS.display.render({
				api_key: config.power_reviews_api_key,
				merchant_group_id: config.power_reviews_merchant_group_id,
				merchant_id: config.power_reviews_merchant_id,
				locale: config.power_reviews_locale,
				page_id: powerreviews_review_details.powerreviews_page_id,
				on_submit: function(config, data) {
      			  jQuery('.pr-custom-header, .required-field').hide();
      			},
				components: {
					Write: powerreviews_review_details.power_reviews_write_review
				}
			});		
		}
	  }
	};
}(jQuery));
