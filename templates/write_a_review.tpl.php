<div class="pr-no-header">
	<div class="pr-custom-header">
		<div class="pr-custom-header-left">
			<img src="<?php echo $data['product_details']['image_url']; ?>">
		</div>
		<div class="pr-custom-header-right">
			<?php if($data['power_reviews']['header_title'] != ''): ?>
				<h1><?php echo $data['power_reviews']['header_title']; ?></h1>
			<?php endif; ?>
			<?php if($data['power_reviews']['header_subtitle'] != ''): ?>
				<h3><?php echo $data['power_reviews']['header_subtitle']; ?></h3>
			<?php endif; ?>
			<p><?php echo $data['product_details']['name']; ?></p>
		</div>
	</div>
	<div class="required-field"><p>*Required fields</p></div>
	<div id="<?php echo $data['power_reviews']['review_id']; ?>" class="pr-content"></div>
</div>